import * as functions from 'firebase-functions';
import { Validators } from './../validators';

export const onCreateUser = functions.database
    .ref('/users/{userId}')
    .onCreate((snap, context) => {
        
        const userId = context.params.userId;
        const user = snap.val();
        console.log(`Novo dado: ${JSON.stringify(user)}`);
        const from = 'no-reply@gym2gym.com.br';
        const userEmail = user.EmailTrabalho;

        console.log(`CNPJ recebido:  ${user.CNPJ} `);

        const userCNPJ = user.CNPJ.toString().replace(".", "").replace(".", "").replace("/", "").replace("-", "");
        console.log(`CNPJ transformado:  ${userCNPJ} `);

        const userName = `${user.Nome} ${user.Sobrenome}`;
           
        const postmarkKey = functions.config().postmark.key;
        const isCnpjValid = Validators.isCnpjValid(userCNPJ);

        const postmark = require("postmark");

        const client = new postmark.Client(postmarkKey);

        console.log(`Enviando email de ${from} para ${userEmail}, nome: ${userName} CNPJ: ${userCNPJ}...`);

        if (isCnpjValid) {
            console.log(`Novo usuário ${userId} cadastrado com sucesso! CNPJ:${userCNPJ}`);

            return client.sendEmail({
                from: from,
                to: userEmail,
                subject: 'Makers - Registro efetuado!',
                HtmlBody: `
                <!doctype html>
                <html>
                    <body> 
                        <div> <strong>${userName},</strong> </div>                        
                        <div> <p> agradecemos ter se registrado conosco! </p> </div>
                        <br>
                        <br>
                        <div> Atenciosamente, </div>
                        <div> Equipe Gym2Gym® </div>
                    </body>
                </html>
                `
            });

        } else {
            console.log(`Usuário ${userId} com CNPJ inválido: ${userCNPJ}`);

            return snap.ref.remove()
                .then(() => {

                    client.sendEmail({
                        from: from,
                        to: userEmail,
                        subject: 'MAKERS - Problemas com seu registro!',
                        HtmlBody: `
                        <!doctype html>
                        <html>
                            <body>
                                <div> <strong>${userName},</strong> </div>                        
                                <div> <p> foi encontrado erro no CNPJ digitado <b>${userCNPJ}</b>. </p> </div>
                                <div> <p> Informamos que ao tentar pedir uma quotação será pedido um novo registro. </p> </div>
                                <br>
                                <br>
                                <div> Atenciosamente, </div>
                                <div> Equipe Gym2Gym® </div>
                            </body>
                        </html>
                        `
                    });

                })
                .catch(error => {
                    console.log(`Falha ao deletar user ${userId} com CNPJ inválido: `, error);
                });
        }

    });