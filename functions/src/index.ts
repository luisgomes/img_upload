import * as users from './users';
import * as config from './config';
import * as admin from 'firebase-admin';

admin.initializeApp(config.firebaseConfig, 'admin');

export const createMakersUser = users.onCreateUser;