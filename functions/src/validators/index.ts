
export class Validators {

    public static isCnpjValid(cnpjString: any) {
        const original_cnpj = cnpjString.toString(); // keep the original input as a string

        const transformed_cnpj = original_cnpj.replace(/[^0-9]/g, ''); // remove everything that is not a number

        let first_numbers_cnpj = transformed_cnpj.slice(0, 12); // get all the 12 first numbers

        /**
         * Multiply the CNPJ
         *
         * @param string    cnpj the CNPJ digits
         * @param int       position the position to begin the regression
         * 
         * @return int      the evaluated digit
         */
        function multiply_cnpj(cnpj: string, pos = 5) {
            let position = pos ? pos : 5;
            let evaluation = 0; // keeps the sum of evaluations

            for (let i = 0; i < cnpj.length; i++) {// for loop through the cnpj string length

                // sum the var by itself then the current CNPJ digit x its position
                evaluation = evaluation + (parseInt(cnpj.charAt(i)) * position);

                // decrease the current position
                position--;

                // if position lower than 2, becomes 9
                if (position < 2) position = 9;
            }
            // returns the sum
            return evaluation;
        }

        // do the first evaluation
        const first_evaluation = multiply_cnpj(first_numbers_cnpj);

        // if the rest of the division between the first calculation and 11 is lower than 2, the first
        // digit is 0, otherwise it is 11 minus the rest of the division of the calculation by 11
        const first_digit = (first_evaluation % 11) < 2 ? 0 : 11 - (first_evaluation % 11);

        // concat the first digit to the  first 12 of the CNPJ
        // now we have 13 numbers here
        first_numbers_cnpj += first_digit;

        // same as first evaluation, starting at position 6
        const second_evaluation = multiply_cnpj(first_numbers_cnpj, 6);
        const second_digit = (second_evaluation % 11) < 2 ? 0 : 11 - (second_evaluation % 11);

        // concat the second digit to the CNPJ
        const evaluated_cnpj = first_numbers_cnpj + second_digit;

        // verify if the input CNPJ and the calculated CNPJ are the same
        if (evaluated_cnpj === transformed_cnpj) return true;

        return false;
    }

}

