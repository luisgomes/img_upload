const functions = require("firebase-functions");

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
// Instruções após a criação da App Web no projeto no Firebase
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDVQbP7JUXkCh9q7GO_-av3Wo3UFcnGPUA",
  authDomain: "makersregistration-b6f11.firebaseapp.com",
  projectId: "makersregistration-b6f11",
  storageBucket: "makersregistration-b6f11.appspot.com",
  messagingSenderId: "159784800375",
  appId: "1:159784800375:web:b80d7a3d05ca6651e23672"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);